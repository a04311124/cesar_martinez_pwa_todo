

document.addEventListener('DOMContentLoaded', function () {
    if ('serviceWorker' in navigator) {
        window.addEventListener('load', function() {
            navigator.serviceWorker.register('sw.js').then(function(registration) {
                console.log('Service Worker registrado con éxito con ámbito:', registration.scope);
            }).catch(function(error) {
                console.log('Error al registrar el Service Worker:', error);
            });
        });
    }

    window.addEventListener('load', function() {
        document.getElementById('splash').style.display = 'block';
        setTimeout(function() {
            document.getElementById('splash').style.display = 'none';
            document.getElementById('home').style.display = 'block';
        }, 3000); // Muestra el Splash durante 3 segundos
    });

   
    const videoElement = document.getElementById('camara');
    const iniciarCamaraButton = document.getElementById('iniciarCamara');
    const detenerCamaraButton = document.getElementById('detenerCamara');
    const descargarVideoButton = document.getElementById('descargarVideo');
    let mediaRecorder;
    let chunks = [];
    
    iniciarCamaraButton.addEventListener('click', function () {
        navigator.mediaDevices.getUserMedia({ video: true })
            .then(function (stream) {
                videoElement.srcObject = stream;
                mediaRecorder = new MediaRecorder(stream);
                mediaRecorder.ondataavailable = function (event) {
                    if (event.data.size > 0) {
                        chunks.push(event.data);
                    }
                };
                mediaRecorder.start();
            })
            .catch(function (error) {
                console.error('Error al acceder a la cámara:', error);
            });
    });
    
    document.getElementById('detenerCamara').addEventListener('click', function () {
        const videoElement = document.getElementById('camara');
        const stream = videoElement.srcObject;
        const tracks = stream.getTracks();
    
        tracks.forEach(function (track) {
            track.stop();
        });
    
        videoElement.srcObject = null;
    });
    
    descargarVideoButton.addEventListener('click', function () {
        if (chunks.length > 0) {
            const blob = new Blob(chunks, { type: 'video/webm' });
            const url = window.URL.createObjectURL(blob);
            const a = document.createElement('a');
            a.style.display = 'none';
            a.href = url;
            a.download = 'video.webm';
            document.body.appendChild(a);
            a.click();
            window.URL.revokeObjectURL(url);
            chunks = [];
        }
    });

    if ('Notification' in window) {
        if (Notification.permission !== 'granted') {
            Notification.requestPermission().then(function (permission) {
                if (permission === 'granted') {
                    console.log('Permisos de notificación concedidos.');
                }
            });
        }
    }
    
    const taskInput = document.getElementById('taskInput');
    const addTaskButton = document.getElementById('addTask');
    const tasksList = document.querySelector('.tasks');

    // Cargar tareas del almacenamiento local al inicio
    loadTasks();

    addTaskButton.addEventListener('click', function () {
        const taskText = taskInput.value.trim();
        if (taskText !== '') {
            // Agregar la tarea al almacenamiento local
            addTask(taskText);
            // Limpiar el campo de entrada
            taskInput.value = '';
        }
    });

    function addTask(taskText) {
        const taskItem = document.createElement('li');
        taskItem.textContent = taskText;

        const editButton = document.createElement('button');
        editButton.textContent = 'Edit';
        taskItem.appendChild(editButton); // Agrega el botón "Editar" a la tarea
        tasksList.appendChild(taskItem);
        const deleteButton = document.createElement('button');
        deleteButton.textContent = 'Eliminar';
        taskItem.appendChild(deleteButton); // Agrega el botón "Eliminar" a la tarea

        const checkButton = document.createElement('input');
        checkButton.className = 'completado';
        checkButton.setAttribute("type", "checkbox");
        taskItem.appendChild(checkButton); // Agrega el Check a la tarea

        saveTask(taskText);

        editButton.addEventListener('click', function () {
            const index = Array.from(taskItem.parentElement.children).indexOf(taskItem);
            const tareas = JSON.parse(localStorage.getItem('tasks')) || [];
            const tareaActual = tareas[index];
            const nuevoTexto = prompt('Edit Taks:', tareaActual);
        
            if (nuevoTexto) {
                tareas[index] = nuevoTexto; // Actualiza el texto de la tarea en el arreglo
                localStorage.setItem('tasks', JSON.stringify(tareas)); // Actualiza el localStorage
                taskItem.textContent = nuevoTexto; // Actualiza el texto en la interfaz
                
            }
        });

        deleteButton.addEventListener('click', function () {
            const index = Array.from(taskItem.parentElement.children).indexOf(taskItem);
            const tareas = JSON.parse(localStorage.getItem('tasks')) || [];
            tareas.splice(index, 1); // Elimina la tarea del arreglo
            localStorage.setItem('tasks', JSON.stringify(tareas)); // Actualiza el localStorage
            taskItem.remove(); // Elimina la tarea de la interfaz
        });

        document.getElementById('lista-tareas').addEventListener('change', function (e) {
            if (e.target.classList.contains('completado')) {
                const tarea = e.target.parentElement;
                const index = tarea.getAttribute('data-index');
                const tareas = JSON.parse(localStorage.getItem('tasks')) || [];
                const tareaActual = tareas[index];
                const completada = e.target.checked; // Verifica si la casilla se ha marcado como completada
        
                if (completada) {
                    tarea.style.textDecoration = 'line-through'; // Aplica estilo de tachado a la tarea completada
                } else {
                    tarea.style.textDecoration = 'none'; // Restaura el estilo normal de la tarea
                }
        
                // Actualiza el estado de completado en el arreglo de tareas y en el localStorage
                tareaActual.completada = completada;
                tareas[index] = tareaActual;
                localStorage.setItem('tasks', JSON.stringify(tareas));
            }
        });
    }

    function saveTask(taskText) {
        let tasks = JSON.parse(localStorage.getItem('tasks')) || [];
        tasks.push(taskText);
        localStorage.setItem('tasks', JSON.stringify(tasks));
        mostrarNotificacion('New Task', taskText);
    }

    function loadTasks() {
        const tasks = JSON.parse(localStorage.getItem('tasks')) || [];
        var indice = 0;
        tasks.forEach(taskText => {
            indice++;
            addTasklist(taskText, indice);
        });
    }

    function addTasklist(taskText,indice) {
        
        
        const taskItem = document.createElement('li');
        taskItem.setAttribute("data-index", indice)
        taskItem.textContent = taskText;

        const editButton = document.createElement('button');
        editButton.textContent = 'Edit';
        taskItem.appendChild(editButton); // Agrega el botón "Editar" a la tarea

        const deleteButton = document.createElement('button');
        deleteButton.textContent = 'Eliminar';
        taskItem.appendChild(deleteButton); // Agrega el botón "Eliminar" a la tarea

        const checkButton = document.createElement('input');
        checkButton.className = 'completado';
        checkButton.setAttribute("type", "checkbox");
        taskItem.appendChild(checkButton); // Agrega el Check a la tarea

        tasksList.appendChild(taskItem);

        editButton.addEventListener('click', function () {
            const index = Array.from(taskItem.parentElement.children).indexOf(taskItem);
            const tareas = JSON.parse(localStorage.getItem('tasks')) || [];
            const tareaActual = tareas[index];
            const nuevoTexto = prompt('Editar la tarea:', tareaActual);
        
            if (nuevoTexto) {
                tareas[index] = nuevoTexto; // Actualiza el texto de la tarea en el arreglo
                localStorage.setItem('tasks', JSON.stringify(tareas)); // Actualiza el localStorage
                taskItem.textContent = nuevoTexto; // Actualiza el texto en la interfaz
                
            }
        });

        deleteButton.addEventListener('click', function () {
            const index = Array.from(taskItem.parentElement.children).indexOf(taskItem);
            const tareas = JSON.parse(localStorage.getItem('tasks')) || [];
            tareas.splice(index, 1); // Elimina la tarea del arreglo
            localStorage.setItem('tasks', JSON.stringify(tareas)); // Actualiza el localStorage
            taskItem.remove(); // Elimina la tarea de la interfaz
        });

       

    }

    document.getElementById('lista-tareas').addEventListener('change', function (e) {
        if (e.target.classList.contains('completado')) {
            const tarea = e.target.parentElement;
            const index = tarea.getAttribute('data-index');
            const tareas = JSON.parse(localStorage.getItem('tasks')) || [];
            const tareaActual = tareas[index];
            const completada = e.target.checked; // Verifica si la casilla se ha marcado como completada
    
            if (completada) {
                tarea.style.textDecoration = 'line-through'; // Aplica estilo de tachado a la tarea completada
            } else {
                tarea.style.textDecoration = 'none'; // Restaura el estilo normal de la tarea
            }
    
            // Actualiza el estado de completado en el arreglo de tareas y en el localStorage
            tareaActual.completada = completada;
            tareas[index] = tareaActual;
            localStorage.setItem('tasks', JSON.stringify(tareas));
        }
    });
   

    function mostrarNotificacion(titulo, mensaje) {
        if (Notification.permission === 'granted') {
            const options = {
                body: mensaje,
                icon: 'imagenes/logo.png', // Puedes especificar una imagen de icono
            };
            const notificacion = new Notification(titulo, options);
        }
    }
});

self.addEventListener('install', function(event) {
    event.waitUntil(
        caches.open('mi-pwa-cache').then(function(cache) {
            return cache.addAll([
                '/PWA/index.html',
                '/PWA/app.js',
                '/PWA/styles.css',
                '/PWA/imagenes/logo.png',
                '/PWA/imagenes/home.jpeg'
            ]);
        })
    );
});

self.addEventListener('fetch', function(event) {

    event.respondWith(
        caches.match(event.request).then(function(response) {
            return response || fetch(event.request);
        })
    );
});

self.addEventListener('push', function (event) {
    const options = {
        body: event.data.text(),
        icon: 'icono-notificacion.png'
        // ... otras opciones
    };

    event.waitUntil(
        self.registration.showNotification('Tarea Pendiente', options)
    );
});